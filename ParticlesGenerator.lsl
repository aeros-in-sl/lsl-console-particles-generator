// Scripts.ParticlesGenerator.ParticlesGenerator.lslp 
// 2016-11-13 06:35:48 - LSLForge (0.1.9.1) generated
// Copyright (c) 2014-2016 Ray Silent


list part_PARTICLE_CONSTANTS = [PSYS_PART_FLAGS,PSYS_SRC_PATTERN,PSYS_SRC_BURST_RADIUS,PSYS_SRC_ANGLE_BEGIN,PSYS_SRC_ANGLE_END,PSYS_SRC_TARGET_KEY,PSYS_PART_START_COLOR,PSYS_PART_END_COLOR,PSYS_PART_START_ALPHA,PSYS_PART_END_ALPHA,PSYS_PART_START_SCALE,PSYS_PART_END_SCALE,PSYS_SRC_TEXTURE,PSYS_SRC_MAX_AGE,PSYS_PART_MAX_AGE,PSYS_SRC_BURST_RATE,PSYS_SRC_BURST_PART_COUNT,PSYS_SRC_ACCEL,PSYS_SRC_OMEGA,PSYS_SRC_BURST_SPEED_MIN,PSYS_SRC_BURST_SPEED_MAX,PSYS_PART_START_GLOW,PSYS_PART_END_GLOW,PSYS_PART_BLEND_FUNC_SOURCE,PSYS_PART_BLEND_FUNC_DEST];
list part_PARTICLE_CONSTANT_NAMES = ["PSYS_PART_FLAGS","PSYS_SRC_PATTERN","PSYS_SRC_BURST_RADIUS","PSYS_SRC_ANGLE_BEGIN","PSYS_SRC_ANGLE_END","PSYS_SRC_TARGET_KEY","PSYS_PART_START_COLOR","PSYS_PART_END_COLOR","PSYS_PART_START_ALPHA","PSYS_PART_END_ALPHA","PSYS_PART_START_SCALE","PSYS_PART_END_SCALE","PSYS_SRC_TEXTURE","PSYS_SRC_MAX_AGE","PSYS_PART_MAX_AGE","PSYS_SRC_BURST_RATE","PSYS_SRC_BURST_PART_COUNT","PSYS_SRC_ACCEL","PSYS_SRC_OMEGA","PSYS_SRC_BURST_SPEED_MIN","PSYS_SRC_BURST_SPEED_MAX","PSYS_PART_START_GLOW","PSYS_PART_END_GLOW","PSYS_PART_BLEND_FUNC_SOURCE","PSYS_PART_BLEND_FUNC_DEST"];
list part_PARTICLE_CONSTANT_TYPES = [TYPE_INTEGER,TYPE_INTEGER,TYPE_FLOAT,TYPE_FLOAT,TYPE_FLOAT,TYPE_KEY,TYPE_VECTOR,TYPE_VECTOR,TYPE_FLOAT,TYPE_FLOAT,TYPE_VECTOR,TYPE_VECTOR,TYPE_STRING,TYPE_FLOAT,TYPE_FLOAT,TYPE_FLOAT,TYPE_INTEGER,TYPE_VECTOR,TYPE_VECTOR,TYPE_FLOAT,TYPE_FLOAT,TYPE_FLOAT,TYPE_FLOAT,TYPE_INTEGER,TYPE_INTEGER];
list part_FLAG_NAMES = ["glow","bounce","wind","ribbon","followSource","followVelocity","targetLinear"];
list part_PARAM_NAMES = ["sysLife","partLife","rate","count","target","radius","push","omega","pattern","angleStart","angleEnd","sizeStart","sizeEnd","speedMin","speedMax","texture","colorStart","colorEnd","alphaStart","alphaEnd","glowStart","glowEnd","blendStart","blendEnd"];


string current;

string part_str_vector2String(vector vector_){
  if ((vector_ == ZERO_VECTOR)) return "<0,0,0>";
  return (((((("<" + part_str_float2String(vector_.x)) + ",") + part_str_float2String(vector_.y)) + ",") + part_str_float2String(vector_.z)) + ">");
}

string part_str_float2String(float float_){
  if ((((integer)float_) == float_)) return ((string)((integer)float_));
  string floatNumber = ((string)float_);
  integer i = (-1);
  while ((llGetSubString(floatNumber,i,i) == "0")) (--i);
  if ((llGetSubString(floatNumber,i,i) == ".")) (--i);
  return llGetSubString(floatNumber,0,i);
}

// Converts string "in" into list with a corresponding value of type "type"
list part_str_string2Typed(string in,integer type){
  if ((type == TYPE_INTEGER)) {
    return [((integer)in)];
  }
  else  if ((type == TYPE_FLOAT)) {
    return [((float)in)];
  }
  else  if ((type == TYPE_VECTOR)) {
    return [((vector)in)];
  }
  else  if ((type == TYPE_ROTATION)) {
    return [((rotation)in)];
  }
  else  if ((type == TYPE_KEY)) {
    return [((key)in)];
  }
  else  {
    return [in];
  }
}

key part_key_forceKey(key in){
  if (in) return in;
  return NULL_KEY;
}

// Returns the number of prims in the object without avatars sitting on it
integer part_link_getNumberOfPrims(){
  integer local0 = llGetObjectPrimCount(llGetKey());
  if ((!local0)) return llGetNumberOfPrims();
  return local0;
}

part_stopAllParticles(){
  integer i;
  integer count = part_link_getNumberOfPrims();
  for ((i = 0); (i < count); (i++)) {
    llLinkParticleSystem((i + (count > 1)),[]);
  }
}

part_setParticlesString(string str){
  llParticleSystem(part_getParticles(str));
}

list part_string2ParticlesNameValueList(string in){
  list parts = llParseString2List(in,["|"],[]);
  list out;
  integer n;
  integer stop = llGetListLength(parts);
  for ((n = 0); (n < stop); (n += 2)) {
    integer constant = ((integer)llList2String(parts,n));
    integer typeindex = llListFindList(part_PARTICLE_CONSTANTS,[constant]);
    if ((typeindex != (-1))) {
      string value = llList2String(parts,(n + 1));
      string name = llList2String(part_PARTICLE_CONSTANT_NAMES,typeindex);
      integer type = llList2Integer(part_PARTICLE_CONSTANT_TYPES,typeindex);
      (out += ([name] + part_str_string2Typed(value,type)));
    }
  }
  return out;
}

string part_getParticlesCode(string current){
  list rawParticles = part_getParticles(current);
  (rawParticles = llListSort(rawParticles,2,TRUE));
  list particles = part_string2ParticlesNameValueList(llDumpList2String(rawParticles,"|"));
  string out;
  integer i;
  for ((i = 0); (i < llGetListLength(particles)); (i += 2)) {
    if ((out != "")) (out += ",\n");
    (out += llList2String(particles,i));
    if (((llGetListEntryType(particles,(i + 1)) == TYPE_STRING) || (llGetListEntryType(particles,(i + 1)) == TYPE_KEY))) {
      (out += ((", \"" + llList2String(particles,(i + 1))) + "\""));
    }
    else  if ((llGetListEntryType(particles,(i + 1)) == TYPE_VECTOR)) {
      (out += (", " + part_str_vector2String(llList2Vector(particles,(i + 1)))));
    }
    else  if ((llGetListEntryType(particles,(i + 1)) == TYPE_FLOAT)) {
      (out += (", " + part_str_float2String(llList2Float(particles,(i + 1)))));
    }
    else  {
      (out += (", " + llList2String(particles,(i + 1))));
    }
  }
  return out;
}

string part_getSorted(string str){
  list result;
  list parts = llParseStringKeepNulls(str,["|"],[]);
  integer i;
  list namesSortList = (part_PARAM_NAMES + part_FLAG_NAMES);
  for ((i = 0); (i < llGetListLength(parts)); (i++)) {
    string part = llList2String(parts,i);
    list nv = llParseStringKeepNulls(part,["="],[]);
    string name = llList2String(nv,0);
    integer nameIndex = llListFindList(namesSortList,[name]);
    (result += [nameIndex,part]);
  }
  (result = llListSort(result,2,TRUE));
  (result = llList2ListStrided(llDeleteSubList(result,0,0),0,65535,2));
  return llDumpList2String(result,"|");
}

integer part_isValidPart(string part,integer verbous){
  list nv = llParseStringKeepNulls(part,["="],[]);
  string name = llList2String(nv,0);
  string value = llList2String(nv,1);
  if ((name == "")) {
    if (verbous) llWhisper(0,(("Wrong command: \"" + name) + "\""));
    return FALSE;
  }
  if ((value == "")) {
    if ((llListFindList(part_FLAG_NAMES,[name]) < 0)) {
      if (verbous) llWhisper(0,(("Wrong flag: \"" + name) + "\""));
      return FALSE;
    }
  }
  else  {
    if ((llListFindList(part_PARAM_NAMES,[name]) < 0)) {
      if (verbous) llWhisper(0,(("Wrong param: \"" + part) + "\""));
      return FALSE;
    }
  }
  return TRUE;
}

integer part_checkParticlesString(string str,integer verbous){
  if ((str == "")) {
    return TRUE;
  }
  integer result = TRUE;
  list parts = llParseStringKeepNulls(str,["|"],[]);
  integer i;
  for ((i = 0); (i < llGetListLength(parts)); (i++)) {
    if ((!part_isValidPart(llList2String(parts,i),verbous))) {
      if (verbous) (result = FALSE);
      else  return FALSE;
    }
  }
  return result;
}

list part_getParticles(string str){
  if ((str == "")) {
    part_stopAllParticles();
    return [];
  }
  list parts = llParseStringKeepNulls(str,["|"],[]);
  integer i;
  integer flags = 0;
  key target = NULL_KEY;
  integer pattern = PSYS_SRC_PATTERN_EXPLODE;
  float radius = 0;
  float angleStart;
  float angleEnd;
  vector colorStart = <1,1,1>;
  vector colorEnd = <(-1),(-1),(-1)>;
  float alphaStart = 1;
  float alphaEnd = 1;
  vector sizeStart = <0,0,0>;
  vector sizeEnd = <(-1),(-1),0>;
  string texture;
  float partLife = 1;
  float sysLife = 1;
  float rate = 0.1;
  integer count = 100;
  vector push;
  vector omega;
  float speedMin = 0.5;
  float speedMax = 0.5;
  float glowStart = 0;
  float glowEnd = 0;
  integer blendFunctionStart = (-1);
  integer blendFunctionEnd = (-1);
  string part;
  string name;
  string value;
  list nv;
  for ((i = 0); (i < llGetListLength(parts)); (i++)) {
    (part = llList2String(parts,i));
    (nv = llParseStringKeepNulls(part,["="],[]));
    (name = llList2String(nv,0));
    (value = llList2String(nv,1));
    list flList = ["glow",PSYS_PART_EMISSIVE_MASK,"bounce",PSYS_PART_BOUNCE_MASK,"wind",PSYS_PART_WIND_MASK,"followSource",PSYS_PART_FOLLOW_SRC_MASK,"followVelocity",PSYS_PART_FOLLOW_VELOCITY_MASK,"ribbon",PSYS_PART_RIBBON_MASK,"targetLinear",PSYS_PART_TARGET_LINEAR_MASK];
    integer flListFound;
    if (((flListFound = llListFindList(flList,[part])) >= 0)) {
      (flags = (flags | llList2Integer(flList,(flListFound + 1))));
    }
    else  if ((name == "blendStart")) {
      (blendFunctionStart = ((integer)value));
    }
    else  if ((name == "blendEnd")) {
      (blendFunctionEnd = ((integer)value));
    }
    else  if ((name == "target")) {
      if ((value == "owner")) {
        (target = llGetOwner());
      }
      else  if ((value == "self")) {
        (target = llGetKey());
      }
      else  {
        (target = part_key_forceKey(((key)value)));
      }
    }
    else  if ((name == "pattern")) {
      integer found = llListFindList(["explode","drop","angleConeEmpty","angleCone","angle"],[value]);
      if ((found >= 0)) {
        (pattern = llList2Integer([PSYS_SRC_PATTERN_EXPLODE,PSYS_SRC_PATTERN_DROP,PSYS_SRC_PATTERN_ANGLE_CONE_EMPTY,PSYS_SRC_PATTERN_ANGLE_CONE,PSYS_SRC_PATTERN_ANGLE],found));
      }
    }
    else  if ((name == "radius")) {
      (radius = ((float)value));
    }
    else  if ((name == "angleStart")) {
      (angleStart = ((float)value));
    }
    else  if ((name == "angleEnd")) {
      (angleEnd = ((float)value));
    }
    else  if ((name == "colorStart")) {
      (colorStart = (((vector)value) / 255.0));
    }
    else  if ((name == "colorEnd")) {
      (colorEnd = (((vector)value) / 255.0));
    }
    else  if ((name == "alphaStart")) {
      (alphaStart = ((float)value));
    }
    else  if ((name == "alphaEnd")) {
      (alphaEnd = ((float)value));
    }
    else  if ((name == "glowStart")) {
      (glowStart = ((float)value));
    }
    else  if ((name == "glowEnd")) {
      (glowEnd = ((float)value));
    }
    else  if ((name == "sizeStart")) {
      (sizeStart = ((vector)value));
      if ((sizeStart.x < 0)) {
        (sizeStart.x = 0);
      }
      if ((sizeStart.y < 0)) {
        (sizeStart.y = 0);
      }
    }
    else  if ((name == "sizeEnd")) {
      (sizeEnd = ((vector)value));
      if ((sizeEnd.x < 0)) {
        (sizeEnd.x = 0);
      }
      if ((sizeEnd.y < 0)) {
        (sizeEnd.y = 0);
      }
    }
    else  if ((name == "texture")) {
      (texture = value);
    }
    else  if ((name == "partLife")) {
      (partLife = ((float)value));
    }
    else  if ((name == "sysLife")) {
      (sysLife = ((float)value));
    }
    else  if ((name == "rate")) {
      (rate = ((float)value));
    }
    else  if ((name == "push")) {
      (push = ((vector)value));
    }
    else  if ((name == "omega")) {
      (omega = ((vector)value));
    }
    else  if ((name == "speedMin")) {
      (speedMin = ((float)value));
    }
    else  if ((name == "speedMax")) {
      (speedMax = ((float)value));
    }
    else  if ((name == "count")) {
      (count = ((integer)value));
    }
  }
  list values;
  list valuesString;
  if ((target != NULL_KEY)) {
    (flags = (flags | PSYS_PART_TARGET_POS_MASK));
    (values += [PSYS_SRC_TARGET_KEY,target]);
  }
  if ((sizeEnd != <(-1),(-1),0>)) {
    (flags = (flags | PSYS_PART_INTERP_SCALE_MASK));
    float LIMIT = 3.125e-2;
    if ((llVecMag(sizeStart) <= llVecMag(sizeEnd))) {
      if ((sizeEnd.x < LIMIT)) (sizeEnd.x = LIMIT);
      if ((sizeEnd.y < LIMIT)) (sizeEnd.y = LIMIT);
    }
    else  {
      if ((sizeStart.x < LIMIT)) (sizeStart.x = LIMIT);
      if ((sizeStart.y < LIMIT)) (sizeStart.y = LIMIT);
    }
    (values += [PSYS_PART_END_SCALE,sizeEnd]);
  }
  if (((colorStart != colorEnd) && (colorEnd != <(-1),(-1),(-1)>))) {
    (flags = (flags | PSYS_PART_INTERP_COLOR_MASK));
    (values += [PSYS_PART_END_COLOR,colorEnd]);
  }
  if ((alphaStart != alphaEnd)) {
    if ((flags & PSYS_PART_INTERP_COLOR_MASK)) {
    }
    else  {
      (values += [PSYS_PART_END_COLOR,colorStart]);
    }
    (flags = (flags | PSYS_PART_INTERP_COLOR_MASK));
  }
  if ((blendFunctionStart >= 0)) {
    (values += [PSYS_PART_BLEND_FUNC_SOURCE,blendFunctionStart]);
  }
  if ((blendFunctionEnd >= 0)) {
    (values += [PSYS_PART_BLEND_FUNC_DEST,blendFunctionEnd]);
  }
  (values += [PSYS_PART_FLAGS,flags,PSYS_SRC_PATTERN,pattern,PSYS_SRC_BURST_RADIUS,radius,PSYS_SRC_ANGLE_BEGIN,angleStart,PSYS_SRC_ANGLE_END,angleEnd,PSYS_PART_START_COLOR,colorStart,PSYS_PART_START_ALPHA,alphaStart,PSYS_PART_END_ALPHA,alphaEnd,PSYS_PART_START_SCALE,sizeStart,PSYS_PART_MAX_AGE,partLife,PSYS_SRC_MAX_AGE,sysLife,PSYS_SRC_BURST_RATE,rate,PSYS_SRC_BURST_PART_COUNT,count,PSYS_SRC_ACCEL,push,PSYS_SRC_OMEGA,omega,PSYS_SRC_BURST_SPEED_MIN,speedMin,PSYS_SRC_BURST_SPEED_MAX,speedMax,PSYS_PART_START_GLOW,glowStart,PSYS_PART_END_GLOW,glowEnd]);
  if ((texture != "")) {
    (values += [PSYS_SRC_TEXTURE,texture]);
  }
  return values;
}

string part_mergeMessage(string current,string message){
  if ((message == "")) return current;
  integer negate = FALSE;
  if ((llGetSubString(message,0,0) == "=")) {
    return llGetSubString(message,1,65535);
  }
  if ((llGetSubString(message,0,0) == "-")) {
    (negate = TRUE);
    (message = llGetSubString(message,1,65535));
  }
  else  if ((llGetSubString(message,0,0) == "+")) {
    (negate = FALSE);
    (message = llGetSubString(message,1,65535));
  }
  else  {
    return current;
  }
  list nv;
  (nv = llParseStringKeepNulls(message,["="],[]));
  string messageName = llList2String(nv,0);
  string messageValue = llList2String(nv,1);
  list parts = llParseStringKeepNulls(current,["|"],[]);
  integer i;
  string part;
  string name;
  string value;
  string result;
  integer found;
  for ((i = 0); (i < llGetListLength(parts)); (i++)) {
    (part = llList2String(parts,i));
    (nv = llParseStringKeepNulls(part,["="],[]));
    (name = llList2String(nv,0));
    (value = llList2String(nv,1));
    if ((name == messageName)) {
      (found = TRUE);
      if ((!negate)) {
        if ((result != "")) (result += "|");
        (result += message);
      }
    }
    else  {
      if ((result != "")) (result += "|");
      (result += name);
      if ((value != "")) (result += ("=" + value));
    }
  }
  if (((!found) && (!negate))) {
    if ((result != "")) (result += "|");
    (result += message);
  }
  return result;
}

clear(){
  (current = "sysLife=1|partLife=1|count=40|rate=0.01|angleStart=3.16|angleEnd=3.14|sizeStart=<0.1,0.1,0>|colorStart=<255,255,255>|speedMin=1|speedMax=2|alphaStart=1|alphaEnd=0|pattern=explode|push=<0,0,-2.81>|wind");
}

default {

  state_entry() {
    llListen(0,"",llGetOwner(),"");
    clear();
    part_stopAllParticles();
  }

  
  link_message(integer sender_num,integer num,string str,key id) {
  }

  
  listen(integer channel,string name,key id,string message) {
    if ((channel != 0)) return;
    string first = llGetSubString(message,0,0);
    if (((first == "=") || (first == "-"))) {
      (current = part_mergeMessage(current,message));
      part_setParticlesString(current);
      llInstantMessage(id,current);
    }
    else  if ((first == "+")) {
      if (part_isValidPart(llGetSubString(message,1,65535),TRUE)) {
        (current = part_mergeMessage(current,message));
        part_setParticlesString(current);
        llInstantMessage(id,current);
      }
    }
    else  if ((message == "p")) {
      if (part_checkParticlesString(current,TRUE)) {
        part_setParticlesString(current);
        llInstantMessage(id,current);
      }
    }
    else  if ((message == "sort")) {
      (current = part_getSorted(current));
      llInstantMessage(id,current);
    }
    else  if ((message == "clear")) {
      clear();
      part_setParticlesString(current);
      llInstantMessage(id,current);
    }
    else  if ((message == "a")) {
      llInstantMessage(id,("\n" + part_getParticlesCode(current)));
    }
    else  if ((((message == "?") || (message == "h")) || (message == "help"))) {
      llInstantMessage(id,((((((((((((((((((((((((((((("Commands:\n" + "p - check & start current particles system\n") + "a - to print array of values\n") + "sort - make command sorted\n") + "clear - to clear all to default values\n") + "? or h - to show this help\n\n") + "Use =<options list> to set new particles system\n") + "Use +<option[=value]> to add new option with optional value\n") + "Use -<option> to remove option\n") + "\n") + "sysLife=0|<duration> (life of particle system)\n") + "partLife=0..30 (life of each particle)\n") + "\n=Flags=:\nglow | bounce | wind | followSource | followVelocity | targetLinear | ribbon\n") + "\n=Params=:\n") + "target=owner|self|<UUID>\n") + "pattern=explode|drop|angle|angleCone|angleConeEmpty|\n") + "radius=0...50\n") + "count=(count of particles)\n") + "rate=0..(any number)\n") + "push=<0..100, 0..100, 0..100>\n") + "omega=<(any number), (any number), (any number)>\n") + "angleStart|End=0..3.14159\n") + "texture=<name>|<UUID>\n") + "colorStart|End=<0..255,0..255,0..255>\n") + "alphaStart|End=0..1\n") + "sizeStart|End=<0.03125..4, 0.03125..4, 0>\n") + "speedMin|Max=(any number)\n") + "glowStart|End=0..1\n") + "blendStart|End=0-5|7|9 - blending functions\n") + "linkName=<primName>"));
    }
  }

  
  touch_start(integer num_detected) {
    integer i;
    for ((i = 0); (i < num_detected); (i++)) {
      if (part_checkParticlesString(current,TRUE)) {
        part_setParticlesString(current);
      }
    }
  }

  
  changed(integer change) {
    if ((change & CHANGED_OWNER)) {
      llResetScript();
    }
    else  if ((change & CHANGED_LINK)) {
      llResetScript();
    }
  }
}
