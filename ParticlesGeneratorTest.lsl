// Copyright (c) 2014 Ray Silent

key pgen_key_forceKey(key in){
    if (in) return in;
    return NULL_KEY;
}

// Returns the number of prims in the object
// without avatars sitting on it
integer pgen_link_getNumberOfPrims(){
    integer local0 = llGetObjectPrimCount(llGetKey());
    if ((!local0)) return llGetNumberOfPrims();
    return local0;
}

pgen_stopAllParticles(){
    integer i;
    integer count = pgen_link_getNumberOfPrims();
    for ((i = 0); (i < count); (i++)) {
        llLinkParticleSystem((i + (count > 1)),[]);
    }
}

pgen_stopParticlesByLinkName(string linkName){
    integer j;
    integer count = pgen_link_getNumberOfPrims();
    for ((j = 0); (j < count); (j++)) {
        if ((llGetLinkName((j + (count > 1))) == linkName)) {
            llLinkParticleSystem((j + (count > 1)),[]);
        }
    }
}

pgen_setParticlesByLinkName(string linkName,string str){
    list particles = pgen_getParticles(str);
    integer j;
    integer primsCount = pgen_link_getNumberOfPrims();
    for ((j = 0); (j < primsCount); (j++)) {
        if ((llGetLinkName((j + (primsCount > 1))) == linkName)) {
            llLinkParticleSystem((j + (primsCount > 1)),particles);
        }
    }
}

pgen_setLinkParticlesString(integer linkNumber,string str){
    llLinkParticleSystem(linkNumber,pgen_getParticles(str));
}

pgen_setParticlesString(string str){
    llParticleSystem(pgen_getParticles(str));
}

list pgen_getParticles(string str){
    if ((str == "")) {
        pgen_stopAllParticles();
        return [];
    }
    list parts = llParseStringKeepNulls(str,["|"],[]);
    integer i;
    integer flags = 0;
    key target = NULL_KEY;
    integer pattern = PSYS_SRC_PATTERN_EXPLODE;
    float radius = 0;
    float angleStart;
    float angleEnd;
    vector colorStart = <1,1,1>;
    vector colorEnd = <(-1),(-1),(-1)>;
    float alphaStart = 1;
    float alphaEnd = 1;
    vector sizeStart = <0,0,0>;
    vector sizeEnd = <(-1),(-1),0>;
    string texture;
    float partLife = 1;
    float sysLife = 1;
    float rate = 0.1;
    integer count = 100;
    vector push;
    vector omega;
    float speedMin = 0.5;
    float speedMax = 0.5;
    float glowStart = 0;
    float glowEnd = 0;
    integer blendFunctionStart = (-1);
    integer blendFunctionEnd = (-1);
    string part;
    string name;
    string value;
    list nv;
    for ((i = 0); (i < llGetListLength(parts)); (i++)) {
        (part = llList2String(parts,i));
        (nv = llParseStringKeepNulls(part,["="],[]));
        (name = llList2String(nv,0));
        (value = llList2String(nv,1));
        list flList = ["glow",PSYS_PART_EMISSIVE_MASK,"bounce",PSYS_PART_BOUNCE_MASK,"wind",PSYS_PART_WIND_MASK,"followSource",PSYS_PART_FOLLOW_SRC_MASK,"followVelocity",PSYS_PART_FOLLOW_VELOCITY_MASK,"ribbon",PSYS_PART_RIBBON_MASK,"targetLinear",PSYS_PART_TARGET_LINEAR_MASK];
        integer flListFound;
        if (((flListFound = llListFindList(flList,[part])) >= 0)) {
            (flags = (flags | llList2Integer(flList,(flListFound + 1))));
        }
        else  if ((name == "blendStart")) {
            (blendFunctionStart = ((integer)value));
        }
        else  if ((name == "blendEnd")) {
            (blendFunctionEnd = ((integer)value));
        }
        else  if ((name == "target")) {
            if ((value == "owner")) {
                (target = llGetOwner());
            }
            else  if ((value == "self")) {
                (target = llGetKey());
            }
            else  {
                (target = pgen_key_forceKey(((key)value)));
            }
        }
        else  if ((name == "pattern")) {
            integer found = llListFindList(["explode","drop","angleConeEmpty","angleCone","angle"],[value]);
            if ((found >= 0)) {
                (pattern = llList2Integer([PSYS_SRC_PATTERN_EXPLODE,PSYS_SRC_PATTERN_DROP,PSYS_SRC_PATTERN_ANGLE_CONE_EMPTY,PSYS_SRC_PATTERN_ANGLE_CONE,PSYS_SRC_PATTERN_ANGLE],found));
            }
        }
        else  if ((name == "radius")) {
            (radius = ((float)value));
        }
        else  if ((name == "angleStart")) {
            (angleStart = ((float)value));
        }
        else  if ((name == "angleEnd")) {
            (angleEnd = ((float)value));
        }
        else  if ((name == "colorStart")) {
            (colorStart = (((vector)value) / 255.0));
        }
        else  if ((name == "colorEnd")) {
            (colorEnd = (((vector)value) / 255.0));
        }
        else  if ((name == "alphaStart")) {
            (alphaStart = ((float)value));
        }
        else  if ((name == "alphaEnd")) {
            (alphaEnd = ((float)value));
        }
        else  if ((name == "glowStart")) {
            (glowStart = ((float)value));
        }
        else  if ((name == "glowEnd")) {
            (glowEnd = ((float)value));
        }
        else  if ((name == "sizeStart")) {
            (sizeStart = ((vector)value));
            if ((sizeStart.x < 0)) {
                (sizeStart.x = 0);
            }
            if ((sizeStart.y < 0)) {
                (sizeStart.y = 0);
            }
        }
        else  if ((name == "sizeEnd")) {
            (sizeEnd = ((vector)value));
            if ((sizeEnd.x < 0)) {
                (sizeEnd.x = 0);
            }
            if ((sizeEnd.y < 0)) {
                (sizeEnd.y = 0);
            }
        }
        else  if ((name == "texture")) {
            (texture = value);
        }
        else  if ((name == "partLife")) {
            (partLife = ((float)value));
        }
        else  if ((name == "sysLife")) {
            (sysLife = ((float)value));
        }
        else  if ((name == "rate")) {
            (rate = ((float)value));
        }
        else  if ((name == "push")) {
            (push = ((vector)value));
        }
        else  if ((name == "omega")) {
            (omega = ((vector)value));
        }
        else  if ((name == "speedMin")) {
            (speedMin = ((float)value));
        }
        else  if ((name == "speedMax")) {
            (speedMax = ((float)value));
        }
        else  if ((name == "count")) {
            (count = ((integer)value));
        }
    }
    list values;
    list valuesString;
    if ((target != NULL_KEY)) {
        (flags = (flags | PSYS_PART_TARGET_POS_MASK));
        (values += [PSYS_SRC_TARGET_KEY,target]);
    }
    if ((sizeEnd != <(-1),(-1),0>)) {
        (flags = (flags | PSYS_PART_INTERP_SCALE_MASK));
        float LIMIT = 3.125e-2;
        if ((llVecMag(sizeStart) <= llVecMag(sizeEnd))) {
            if ((sizeEnd.x < LIMIT)) (sizeEnd.x = LIMIT);
            if ((sizeEnd.y < LIMIT)) (sizeEnd.y = LIMIT);
        }
        else  {
            if ((sizeStart.x < LIMIT)) (sizeStart.x = LIMIT);
            if ((sizeStart.y < LIMIT)) (sizeStart.y = LIMIT);
        }
        (values += [PSYS_PART_END_SCALE,sizeEnd]);
    }
    if (((colorStart != colorEnd) && (colorEnd != <(-1),(-1),(-1)>))) {
        (flags = (flags | PSYS_PART_INTERP_COLOR_MASK));
        (values += [PSYS_PART_END_COLOR,colorEnd]);
    }
    if ((alphaStart != alphaEnd)) {
        if ((flags & PSYS_PART_INTERP_COLOR_MASK)) {
        }
        else  {
            (values += [PSYS_PART_END_COLOR,colorStart]);
        }
        (flags = (flags | PSYS_PART_INTERP_COLOR_MASK));
    }
    if ((blendFunctionStart >= 0)) {
        (values += [PSYS_PART_BLEND_FUNC_SOURCE,blendFunctionStart]);
    }
    if ((blendFunctionEnd >= 0)) {
        (values += [PSYS_PART_BLEND_FUNC_DEST,blendFunctionEnd]);
    }
    (values += [PSYS_PART_FLAGS,flags,PSYS_SRC_PATTERN,pattern,PSYS_SRC_BURST_RADIUS,radius,PSYS_SRC_ANGLE_BEGIN,angleStart,PSYS_SRC_ANGLE_END,angleEnd,PSYS_PART_START_COLOR,colorStart,PSYS_PART_START_ALPHA,alphaStart,PSYS_PART_END_ALPHA,alphaEnd,PSYS_PART_START_SCALE,sizeStart,PSYS_PART_MAX_AGE,partLife,PSYS_SRC_MAX_AGE,sysLife,PSYS_SRC_BURST_RATE,rate,PSYS_SRC_BURST_PART_COUNT,count,PSYS_SRC_ACCEL,push,PSYS_SRC_OMEGA,omega,PSYS_SRC_BURST_SPEED_MIN,speedMin,PSYS_SRC_BURST_SPEED_MAX,speedMax,PSYS_PART_START_GLOW,glowStart,PSYS_PART_END_GLOW,glowEnd]);
    if ((texture != "")) {
        (values += [PSYS_SRC_TEXTURE,texture]);
    }
    return values;
}

default {
  state_entry() {
    // Stopping particles convenient wrappers
    pgen_stopAllParticles(); // Walks through all linkset to stop particles
    pgen_stopParticlesByLinkName("Object");
    
    // Example of modifications in the generator output string using a variables
    vector colorStart=<llFrand(255),100,50>;
    vector colorEnd=<llFrand(255),100,50>;
    string particlesGeneratorOutput="sysLife=1|partLife=1|count=40|rate=0.01|angleStart=3.16|angleEnd=3.14|sizeStart=<0.1,0.1,0>"
                                  +"|colorStart="+(string)colorStart
                                  +"|colorEnd="+(string)colorStart                                  
                                  +"|speedMin=1|speedMax=2|alphaStart=1|alphaEnd=0|pattern=explode|push=<0,0,-2.81>|wind";
    
    // Passing particle generator output directly by several wrapper methods
    pgen_setParticlesByLinkName("Object", particlesGeneratorOutput);
    pgen_setLinkParticlesString(1, particlesGeneratorOutput);
    pgen_setParticlesString(particlesGeneratorOutput);
    
    // Just Parsing particle generator output for passing to ll function
    list particles=pgen_getParticles(particlesGeneratorOutput);
    llParticleSystem(particles);

  }
}
