# [Aeros](https://bitbucket.org/aeros-in-sl/) :: **[LSL Console Particles Generator](https://bitbucket.org/aeros-in-sl/lsl-console-particles-generator/)**
This is a console particles generator which helps to play with particles and subsequently generate arrays of LSL values or get a Source string containing all necessary data for later improvements or run-time manipulation.

Go to [wiki](https://bitbucket.org/aeros-in-sl/lsl-console-particles-generator/wiki/Home) for details